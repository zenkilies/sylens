import React from 'react';

import "src/styles/app.scss";

function MyApp({Component, pageProps}) {
  return (
    <React.Fragment>
      <Component {...pageProps} />
    </React.Fragment>
  );
}

export default MyApp;
