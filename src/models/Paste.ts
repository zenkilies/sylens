import HttpHelper, {IResponse} from "src/helpers/HttpHelper";

class Paste {
  static async createPaste(params: ICreatePasteParams): Promise<IResponse> {
    return await HttpHelper.post(`/pastes`, {
      "content": params.content,
      "password": params.password ? params.password : "",
    }, {
      withReCAPTCHA: true,
    });
  }

  static async retrievePaste(slug: string): Promise<IResponse> {
    return await HttpHelper.get(`/pastes/${slug}`);
  }

  static async retrieveProtectedPaste(slug: string, password: string): Promise<IResponse> {
    return await HttpHelper.post(`/pastes/${slug}/unlock`, {
      "password": password,
    }, {
      withReCAPTCHA: true,
    });
  }
}

export default Paste;

export declare interface IPasteSchema {
  slug: string;
  content: string;
}

declare interface ICreatePasteParams {
  content: string;
  password?: string;
}
