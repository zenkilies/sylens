import Head from "next/head";
import React from "react";

import Footer from "src/components/Footer/Footer";
import Navbar from "src/components/Navbar/Navbar";

import PasteForm from "./components/PasteForm/PasteForm";

class HomePage extends React.Component<any, any> {
  render() {
    return (
      <section id="home">
        <Head>
          <title>Sylens</title>
          <script src={"https://www.google.com/recaptcha/api.js?render=" + process.env.RECAPTCHA_KEY}/>
        </Head>

        <Navbar/>

        <div className="uk-section">
          <div className="uk-container uk-container-small">
            <p className="uk-text-small uk-text-justify">
              This site is intended for use as a short-term exchange of pasted information between parties.
              All submitted data is considered public information. Submitted data is not guaranteed to be permanent,
              and may be removed at any time. Please do not set up programs to send data to this site in an automated
              fashion; it is intended to be used directly by humans.
            </p>

            <PasteForm/>
          </div>
        </div>

        <Footer/>
      </section>
    );
  }
}

export default HomePage;
