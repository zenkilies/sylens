import _ from "lodash";
import React from "react";
import {withRouter} from "next/router";

import Paste from "src/models/Paste";

declare interface IState {
  hasPassword: boolean;
  isLoading: boolean;
}

class PasteForm extends React.Component<any, IState> {
  private readonly contentRef;
  private readonly passwordRef;

  constructor(props) {
    super(props);

    this.state = {
      hasPassword: false,
      isLoading: false,
    };

    this.contentRef = React.createRef();
    this.passwordRef = React.createRef();

    this.toggleLoading = this.toggleLoading.bind(this);
    this.onChangeHasPassword = this.onChangeHasPassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  toggleLoading(isLoading = false) {
    this.setState({
      isLoading: isLoading,
    });
  }

  onChangeHasPassword(e: any) {
    this.setState({
      hasPassword: e.target.checked,
    });
  }

  async onSubmit(e: any) {
    e.preventDefault();

    const content = this.contentRef.current.value;
    const password = this.passwordRef.current ? this.passwordRef.current.value : "";

    this.toggleLoading(true);

    const {data, status} = await Paste.createPaste({
      content: content,
      password: password,
    });
    if (status !== 200) {
      alert("Something went wrong, please try again!");
      this.toggleLoading(false);
      return;
    }

    const slug = _.get(data, "slug", "");
    if (slug == "") {
      alert("Something went wrong, please try again!");
      this.toggleLoading(false);
      return;
    }

    this.props.router.push(`/p/${slug}`).catch(console.error());
  }

  render() {
    const {hasPassword, isLoading} = this.state;

    return (
      <form className="uk-form-stacked uk-margin-medium-top" onSubmit={this.onSubmit}>
        <div className="uk-margin-bottom">
          <div className="uk-form-controls">
            <textarea className="uk-textarea uk-border-rounded uk-padding" rows={12} ref={this.contentRef} required/>
          </div>
        </div>

        <div className="uk-margin-bottom uk-grid uk-flex-between uk-flex-middle">
          <label>
            <input className="uk-checkbox uk-border-rounded uk-margin-small-right" type="checkbox" checked={hasPassword} onChange={this.onChangeHasPassword}/>
            <span className="uk-text-small">Password Protecting</span>
          </label>

          {hasPassword === false ? "" : (
            <div className="uk-form-control uk-flex-1">
              <input className="uk-input uk-border-rounded" type="password" placeholder="Password" ref={this.passwordRef} required/>
            </div>
          )}
        </div>

        <div className="uk-text-right">
          <button type="submit" className="uk-button uk-button-primary uk-border-rounded" disabled={isLoading}>
            Create
          </button>
        </div>
      </form>
    );
  }
}

export default withRouter(PasteForm);
