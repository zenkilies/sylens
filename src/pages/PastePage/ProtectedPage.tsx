import Head from "next/head";
import Link from "next/link";
import React, {RefObject} from "react";

import Footer from "src/components/Footer/Footer";
import Navbar from "src/components/Navbar/Navbar";
import Paste, {IPasteSchema} from "src/models/Paste";

import PublicPage from "./PublicPage";

declare interface IProps {
  slug: string;
}

declare interface IState {
  data?: IPasteSchema;
  isLoading: boolean;
}

class ProtectedPage extends React.Component<IProps, IState> {
  private readonly passwordRef: RefObject<any>;

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      isLoading: false,
    };

    this.passwordRef = React.createRef();

    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(e: any) {
    e.preventDefault();

    this.setState({
      isLoading: true,
    });

    const slug = this.props.slug;
    const password = this.passwordRef.current.value;

    const {data, status} = await Paste.retrieveProtectedPaste(slug, password);

    if (status !== 200) {
      alert("Password is mis-matched, please try again!");

      this.setState({
        isLoading: false,
      });

      return;
    }

    this.setState({
      data: data,
      isLoading: false,
    });
  }

  render() {
    const {data, isLoading} = this.state;
    if (data !== null) {
      return <PublicPage data={data}/>
    }

    return (
      <section id="paste">
        <Head>
          <title>Sylens</title>
          <script src={"https://www.google.com/recaptcha/api.js?render=" + process.env.RECAPTCHA_KEY}/>
        </Head>

        <Navbar/>

        <div className="uk-section">
          <div className="uk-container container-xxsmall">
            <form className="uk-form-stacked" onSubmit={this.onSubmit}>
              <div className="uk-margin-medium-bottom">
                <div className="uk-form-controls">
                  <input className="uk-input uk-form-large uk-border-rounded" type="password" placeholder="Password" ref={this.passwordRef} required/>
                </div>
              </div>

              <div>
                <button type="submit" className="uk-button uk-button-secondary uk-button-large uk-border-rounded uk-width-1-1" disabled={isLoading}>
                  Unlock
                </button>
              </div>
            </form>

            <div className="uk-margin-medium-top paginate-post">
              <Link href="/">
                <a className="remove-underline hvr-back" href="/">
                  &larr; <span className="uk-margin-left">Create New Paste</span>
                </a>
              </Link>
            </div>
          </div>
        </div>

        <Footer/>
      </section>
    );
  }
}

export default ProtectedPage;
