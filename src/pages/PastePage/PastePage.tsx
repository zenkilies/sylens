import _ from "lodash";
import Error from "next/error";
import React from "react";

import Paste, {IPasteSchema} from "src/models/Paste";

import ProtectedPage from "./ProtectedPage";
import PublicPage from "./PublicPage";

declare interface IProps {
  slug: string;
  data?: IPasteSchema;
  status: number;
}

class PastePage extends React.Component<IProps, any> {
  render() {
    const {slug, data, status} = this.props;

    if (status === 404) {
      return <Error statusCode={404}/>
    }

    if (status === 401) {
      return <ProtectedPage slug={slug}/>
    }

    if (status !== 200) {
      return <Error statusCode={500}/>
    }

    return (
      <PublicPage data={data}/>
    );
  }
}

export default PastePage;

export async function getServerSideProps(context) {
  const {data, status} = await Paste.retrievePaste(_.get(context, "params.slug"));

  return {
    props: {
      slug: _.get(context, "params.slug"),
      data: data,
      status: status,
    }
  };
}
