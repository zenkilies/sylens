import Head from "next/head";
import Link from "next/link";
import React from "react";

import Footer from "src/components/Footer/Footer";
import Navbar from "src/components/Navbar/Navbar";
import {IPasteSchema} from "src/models/Paste";

declare interface IProps {
  data: IPasteSchema;
}

class PublicPage extends React.Component<IProps, any> {
  render() {
    return (
      <section id="paste">
        <Head>
          <title>Sylens</title>
        </Head>

        <Navbar/>

        <div className="uk-section">
          <div className="uk-container uk-container-small">
            <pre className="uk-padding uk-text-secondary" style={{whiteSpace: "pre-wrap"}}>
              {this.props.data.content}
            </pre>

            <div className="uk-margin-medium-top paginate-post">
              <Link href="/">
                <a className="remove-underline hvr-back" href="/">
                  &larr; <span className="uk-margin-left">Create New Paste</span>
                </a>
              </Link>
            </div>
          </div>
        </div>

        <Footer/>
      </section>
    );
  }
}

export default PublicPage;
