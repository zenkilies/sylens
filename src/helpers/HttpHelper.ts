import _ from "lodash";
import axios from "axios";

axios.defaults.withCredentials = true;

export declare interface IOptions {
  withReCAPTCHA?: boolean;
}

export declare interface IResponse {
  data: any | null;
  status: number;
}

class HttpHelper {
  static async get(url: string): Promise<IResponse> {
    url = process.env.API_HOST + url;

    try {
      const {data, status} = await axios.get(url);

      return {
        data: data,
        status: status,
      };
    } catch (error) {
      const data = _.get(error, "response.data", null);
      const status = _.get(error, "response.status", 0);

      return {
        data: data,
        status: status,
      };
    }
  }

  static async post(url: string, payload: any, options: IOptions = {}): Promise<IResponse> {
    url = process.env.API_HOST + url;

    if (options.withReCAPTCHA) {
      const token = await grecaptchaToken();
      if (token !== null) {
        _.set(payload, "_grecaptcha", token);
      }
    }

    try {
      const {data, status} = await axios.post(url, payload);

      return {
        data: data,
        status: status,
      };
    } catch (error) {

      const data = _.get(error, "response.data", null);
      const status = _.get(error, "response.status", 0);

      return {
        data: data,
        status: status,
      };
    }
  }
}

export default HttpHelper;

const grecaptchaToken = async function (): Promise<string | null> {
  try {
    const grecaptcha = window["grecaptcha"];
    if (typeof grecaptcha !== 'object') {
      return null;
    }

    return await grecaptcha.execute(process.env.RECAPTCHA_KEY);
  } catch (error) {
    console.error(error);
    return null;
  }
}
